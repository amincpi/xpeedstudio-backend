<?php
    namespace Config;
    use Config\Connection;
    use App\helper\RequestCustom;
    class Query extends Connection{
        public $select_columns="*";
        public $condi=[];
        public $condi_values=[];public $condi_between=[];
        public $orderBy="";
        public $between="";
        public function insert($data){
            $columnString = implode(',', array_keys($data));
            $valueString = implode(',', array_fill(0, count($data), '?'));
            $statm=$this->conn->prepare("INSERT INTO {$this->table} ({$columnString}) VALUES ({$valueString})");
            $statm->execute(array_values($data));
            return $this->conn->lastInsertId();
        }
        public function select($cols){
           $this->select_columns=$cols;
           return $this;
        }
        public function orderBy($col,$val){
           $this->orderBy=" order by {$col} {$val}";
           return $this;
        }
        public function whereBetween($col,$values=[]){
            $this->condi_between[]=" {$col} BETWEEN {$values[0]} and {$values[1]}";
        }
        public function where($condi,$param=null,$param2=null){
            if(is_array($condi)){
                $where="";
                foreach($condi as $key => $val){
                    $this->condi[]="{$key}=?";
                    $this->condi_values[]=$val;
                }
                return $this;
            }
            else if(!empty($param)){
                $symbol="=";
                if(!empty($param2))
                    {
                        $symbol=$param;
                        $param=$param2;
                    }

                $this->condi[]="{$condi} {$symbol}?";
                $this->condi_values[]=$param;
            }
            else
                $condi($this);
            return $this;
        }
        public function get(){
            $where="";$whereBetween="";
            if(count($this->condi) > 0)
                $where= "And ".implode("And ",$this->condi);
            if(count($this->condi_between) > 0)
                $whereBetween= "And ".implode("And ",$this->condi_between);
                
            $sql="select {$this->select_columns} from {$this->table} where 1 {$where} {$whereBetween} {$this->orderBy}";
            //return $sql;
            $stmt = $this->conn->prepare($sql);
            //if(count($this->condi_values) > 0)
            $stmt->execute($this->condi_values);
            $res=$stmt->fetchAll(\PDO::FETCH_ASSOC);
            $this->closeConnection();
            return $res;
        }
        
    }

?>