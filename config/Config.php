<?php
    $cookie_options = array(
        'expires' => time() + 600,
        'path' => '/',
        'domain' => 'https://xpeed.studio.infoagebd.com', // leading dot for compatibility or use subdomain
        'secure' => true, // or false
        'httponly' => false, // or false
        'samesite' => 'None' // None || Lax || Strict
      );
    define("salt_key","salt");
    define("cookie_name","auth");
    define("cookie_time",600);
    define("cookie_config",$cookie_options);
    date_default_timezone_set('Asia/Dhaka');
    define("cookie_msg","Submission is blocked for 24hours since submission time");
    define("cookie_value",true);
    define('SITE_NAME', 'XpeedStudio');
    define('APP_ROOT', dirname(dirname(__FILE__)));
    define('URL_ROOT', '/');
    define('URL_SUBFOLDER', '/xpeedstudio-backend');
?>