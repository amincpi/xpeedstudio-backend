<?php
    namespace Config;
    class Connection{    
        protected $conn=null;
        protected $host="localhost";
        protected $db="infoekod_xpeed_orders";
        protected $user="root";
        protected $password="";
        //protected $class=null;
        public function __construct($class=null){
            try {
                //$this->class=$class;
                $dsn = "mysql:host=$this->host;dbname=$this->db;charset=UTF8";
                $options = [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION];
	            $this->conn = new \PDO($dsn, $this->user, $this->password, $options);
              } catch(\PDOException $e) {
                    echo "Connection failed: " . $e->getMessage();
              }
        }
        public function closeConnection() {
            $this->conn = null;
         }   
    }

?>