<?php 
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
// Routes system
$routes = new RouteCollection();
$routes->add('order_store', new Route(constant('URL_SUBFOLDER') . '/order/store', 
    array('controller' => 'OrderController', 'method'=>'store'),  array(), array(), '', array(), array('POST')));

$routes->add('order_list', new Route(constant('URL_SUBFOLDER') . '/order/list', 
    array('controller' => 'OrderController', 'method'=>'fetch'), array(), array(), '', array(), array('GET')));
