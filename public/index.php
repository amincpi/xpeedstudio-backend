<?php
    session_start();
    require_once '../config/Config.php';
    require_once '../config/Cors.php';
    require_once '../vendor/autoload.php';
    require_once '../routes/web.php';
    require_once '../app/Router.php';
?>