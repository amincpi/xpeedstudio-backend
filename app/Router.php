<?php 

namespace App;

use App\helper\RequestCustom;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\NoConfigurationException;
class Router
{
    public function __invoke(RouteCollection $routes)
    {

       
        if(isset($_COOKIE[constant("cookie_name")])) {
            http_response_code(500);
            echo json_encode([constant("cookie_msg")]);
            exit(); 
        }
        
        $request=Request::createFromGlobals();
        $path = $request->getPathInfo();
        $context = new RequestContext();
        $context->fromRequest($request);
       
        $customReq=new RequestCustom($request);
       
        // Routing can match routes with incoming requests
        $matcher = new UrlMatcher($routes, $context);
        try {
            //$matcher = $matcher->match($_SERVER['REQUEST_URI']);
            $matcher = $matcher->match($path);
            
            // Cast params to int if numeric
            array_walk($matcher, function(&$param)
            {
                if(is_numeric($param)) 
                {
                    $param = (int) $param;
                }
            });
            
            $className = '\\App\\controllers\\' . $matcher['controller'];
            // print_r($className);
            // exit();
            $classInstance = new $className();
            // Add routes as paramaters to the next class
            //$params = array_merge(array_slice($matcher, 2, -1), array('routes' => $routes));
            
            call_user_func_array(array($classInstance, $matcher['method']), [$customReq]);
            
        } catch (MethodNotAllowedException $e) {
            echo 'Route method is not allowed.';
        } catch (ResourceNotFoundException $e) {
            echo 'Route does not exists.';
             http_response_code(404);
             echo json_encode(["Route does not exists"]);
        } catch (NoConfigurationException $e) {
            echo 'Configuration does not exists.';
        }
    }
}

// Invoke
$router = new Router();
$router($routes);
