<?php
    namespace App\helper;
    use Symfony\Component\HttpFoundation\Request;
    class RequestCustom{
        public $req=null;public $errors=[];public $ConvToDbDate=null;
        public function __construct(Request $req){
            $this->req=$req;
        }
        public function setError($message){
            $this->errors[]=$message;
        }
        public function ConvToDbDate($date){
            return $this->ConvToDbDate=date('Y-m-d',strtotime(trim($date)));
        }
        public static function isValidEmail($email){
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        }
        public static function isOnlyValidNumber($number){
            return !is_numeric($number) ? false : true;
        }
        public static function isValidNumber($number){
            return preg_match('/[0-9]/', $number);
        }
        public static function isOnlyText($text){
            //return !is_numeric($number) ? true : false;
            return !ctype_digit($text) ? true : false;
        }
        public static function isValidText($text){
            return preg_match('/[A-Za-z]/', $text);
        }
        public function validation($rules){
            $req_data=(object)$this->req->toArray();
            foreach($rules as $key => $val){
                if(empty($req_data->$key))
                    {
                        self::setError([$key => $key." is required!"]);
                        break;
                    }
                else{
                    $type_exp=isset($val["type"]) ? explode("|",$val["type"]) : [];
                    $len=count($type_exp);
                    if($len > 0 && !empty($type_exp[1])){
                        $status=false;
                        for($i=0;$i<$len;$i++){
                            $dynFun="isValid".ucfirst($type_exp[$i]);
                            $status=self::$dynFun($req_data->$key);
                            if($status){
                                $status=true;
                                break;
                            }
                        }
                        $status ? null : self::setError([$key => $key." is not valid!"]);

                        
                    }
                    else if(isset($val["type"])){
                        if($val["type"] == 'array'){
                            $ara_len=count($req_data->$key);
                            $ara_len > 0 && is_array($req_data->$key) ? null :self::setError([$key => $key." is required!"]);
                        }
                        else if($val["type"] == 'phone'){
                            $phone_len=strlen($req_data->$key);
                            self::isOnlyValidNumber($req_data->$key) && $phone_len == 13 ? null : self::setError("Valid phone is required!");;
                        }
                        else if($val["type"] == 'number'){
                            self::isOnlyValidNumber($req_data->$key) ? null : self::setError($key." is not a number!");
                        }
                        else if($val["type"] == 'email'){
                            self::isValidEmail($req_data->$key) ? null : self::setError("Valid email is required!");
                        }
                        else if($val["type"] == 'text'){
                            self::isOnlyText($req_data->$key) ? null : self::setError(" Only text is allow for ".$key);
                        }
                    }

                    if(isset($val["max_len"])){
                        $len=strlen($req_data->$key);
                        if($len > $val["max_len"])
                            {
                                self::setError($key." field is max {$val["max_len"]} words allow!");
                                break;
                            }
                    }
                }
            }
            if(count($this->errors) > 0){
                http_response_code(403);
                echo json_encode($this->errors);
                exit();
            }
            return null;   
        }
    }
?>