<?php 
    namespace App\Controllers;
    use App\helper\RequestCustom;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Routing\RouteCollection;
    use App\models\OrderModel;
    class OrderController{
        protected $OrderModel=null;
        public function __construct(){
            $this->OrderModel=new OrderModel();
        }
        public function fetch(RequestCustom $request){
            $reqQuery=$req_data=$request->req->query;
            // echo $request->ConvToDbDate($reqQuery->get("date_from"));
            // exit();
            $list=$this->OrderModel
                ->where(function($q) use($reqQuery,$request){
                    if(!empty($reqQuery->get("date_from")))
                        $q->where("entry_at",">=",$request->ConvToDbDate($reqQuery->get("date_from")));
                    if(!empty($reqQuery->get("date_to")))
                        $q->where("entry_at","<=",$request->ConvToDbDate($reqQuery->get("date_to")));
                    if(!empty($reqQuery->get("user_id")))
                        $q->where("entry_by",$reqQuery->get("user_id"));
                })
                ->orderBy("id","desc")
                ->get();
            echo json_encode(["status" => 1,"result" => $list]);
        }
        public function store(RequestCustom $request){
            $salt=constant("salt_key");
            $req_data=(object)$request->req->toArray();
            $rules=[
                "amount" => ["type"=> "number"],
                "buyer" => ["type"=> "text|number","max_len" => 20],
                "receipt_id" => ["type"=> "text"],
                "items" => ["type"=> "array"],
                "buyer_email" => ["type"=> "email"],
                "note" => ["type"=> "text","max_len" => 30],
                "city" => ["type"=> "text"],
                "phone" => ["type"=> "phone"],
                "entry_by" => ["type"=> "number"],
            ];
            $request->validation($rules);
            $ara_insert=[
                    "amount" => $req_data->amount,
                    "buyer" => $req_data->buyer,
                    "receipt_id" => $req_data->receipt_id,
                    "items" => implode(",",$req_data->items),
                    "buyer_email" => $req_data->buyer_email,
                    "buyer_ip" => $request->req->getClientIp(),
                    "note" => $req_data->note,
                    "city" => $req_data->city,
                    "phone" => $req_data->phone,
                    "hash_key" => hash ( "sha512", $salt . $req_data->receipt_id),
                    "entry_at" => date('Y-m-d'),
                    "entry_by" => $req_data->entry_by,
            ];
            try {
                $this->OrderModel->insert($ara_insert);
                $data=$this->OrderModel->orderBy("id","desc")->get();
                $ctime=time() + constant("cookie_time");
                setcookie(constant("cookie_name"), constant("cookie_value"),['samesite' => 'None', 'secure' => true]);
                echo json_encode(["cc"=> $_COOKIE ,"status" => 1,"result" => $data]);
            }
            catch(\Exception $e) {
                // http_response_code(500);
                 echo json_encode(["Internal Server Error .". $e->getMessage()]); 
            }
        }
    }   
?>